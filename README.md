<h1 align="center">𝐇𝐞𝐲 👋 𝗩𝚵𝐍Ⓞ𝐌 𝐡𝐞𝐫𝐞.</h1>
<h3 align="center">A Passionate Developer & Engineering Student from India.</h3>
<img align="right" alt="Coding" width="400" src="https://media3.giphy.com/media/qgQUggAC3Pfv687qPC/giphy.webp?cid=6c09b952urwjww849sjzp4bj7defp2oogtwnnzdhmyozmsu8&ep=v1_internal_gif_by_id&rid=giphy.webp&ct=g">


![Hits](https://hits.seeyoufarm.com/api/count/incr/badge.svg?url=https%3A%2F%2Fgithub.com%2Fpirate303&count_bg=%23DDDDDD&title_bg=%23000000&icon=apachecassandra.svg&icon_color=%23E7E7E7&title=Pirate%20Hits&edge_flat=false)

- 🔭 I’m currently working on Bot Development Projects.

- 🌱 I’m currently learning **C++ , Java , DSS.**

- 👨‍💻 All of my projects will be available here soon.

- 💬 Ask me about **Bots Development, Web Development, Web Designing, Logo Designing e.t.c**

- 📫 How to reach me **Pirate303team@gmail.com**

- 📄 My Development Experiences will be available Here Soon. 

- ⚡ Fun fact **I am in love with My Tech stuffs since 2014.**

#
  
</p>
<img align="left" height="300px" width="300px" alt="𝙶𝙸𝙵" src="https://camo.githubusercontent.com/3b7c592ede97b6138ffd4b1cc1541c2f3b11fd39/687474703a2f2f33312e6d656469612e74756d626c722e636f6d2f31376665613932306666333665663466356238373764353231366137616164392f74756d626c725f6d6f39786a65387a5a34317163626975666f315f313238302e676966"/>
<br/>

<h3 align="left">Languages and Tools:</h3>

<br/>


<code><img height="40" width="40" src="https://www.naveedashfaq.me/img/python.png"/></code>
<code><img height="40" width="40" src="https://www.naveedashfaq.me/img/html.png"/></code>
<code><img height="40" width="40" src="https://www.naveedashfaq.me/img/java.png"/></code>
<code><img height="40" width="40" src="https://www.naveedashfaq.me/img/c.png"/></code>
<code><img height="40" width="40" src="https://www.naveedashfaq.me/img/c++.png"/></code>
<code><img height="40" width="40" src="https://www.naveedashfaq.me/img/linux.png"/></code>
<code><img height="40" width="40" src="https://icon-library.com/images/heroku-icon/heroku-icon-27.jpg"/></code>
<code><img height="40" width="40" src="https://www.naveedashfaq.me/img/git.png"/></code>
<code><img height="40" width="40" src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Github-desktop-logo-symbol.svg/1200px-Github-desktop-logo-symbol.svg.png"/></code>
<code><img height="40" width="40" src="https://ratfactor.com/misc/mingw64/mingw-w64.svg"/></code>
<code><img height="40" width="40" src="https://www.naveedashfaq.me/img/unity.png"/></code>

##

<code><img height="40" width="40" src="https://i.pinimg.com/originals/5c/98/5e/5c985e0b67da1a2f01c07a30996f128f.png"/></code>
<code><img height="40" width="40" src="https://upload.wikimedia.org/wikipedia/commons/5/50/Adobe_Illustrator_icon.png"/></code>
<code><img height="40" width="40" src="https://upload.wikimedia.org/wikipedia/commons/1/1c/Adobe_Express_logo_RGB_1024px.png"/></code>
<code><img height="40" width="40" src="https://freelogopng.com/images/all_img/1656733637logo-canva-png.png"/></code>
<code><img height="40" width="40" src="https://cdn.icon-icons.com/icons2/3053/PNG/512/photoscape_x_macos_bigsur_icon_189834.png"/></code>
<code><img height="40" width="40" src="https://brandslogos.com/wp-content/uploads/images/large/arduino-logo-1.png"/></code>
<code><img height="40" width="40" src="https://static-00.iconduck.com/assets.00/visual-studio-code-icon-2048x2026-9ua8vqiy.png"/></code>
<code><img height="40" width="40" src="https://i.pinimg.com/originals/6f/73/32/6f73329230883dde8838c8f67479413f.png"/></code>
<code><img height="40" width="40" src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/Garuda-blue-sgs.svg/1200px-Garuda-blue-sgs.svg.png"/></code>
<code><img height="40" width="40" src="https://images.rawpixel.com/image_png_social_square/czNmcy1wcml2YXRlL3Jhd3BpeGVsX2ltYWdlcy93ZWJzaXRlX2NvbnRlbnQvbHIvdjk4Mi1kMi0yMy5wbmc.png"/></code>
<code><img height="40" width="40" src="https://w7.pngwing.com/pngs/625/558/png-transparent-bing-bing-logo-logo-search-engine-brand-logo-linux-logo-3d-icon-thumbnail.png"/></code>

#
